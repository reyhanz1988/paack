/* eslint-disable jest/expect-expect */
/* eslint-disable jest/no-commented-out-tests */
/* eslint-disable no-undef */

beforeAll(async () => {
	await device.launchApp();
});
const testFn = () => {
	//HOME already test rendered after switch language 
	//INSURANCE
	it("Render InsuranceScreen and open ProductsScreen as initial screen test", async () => {
		await element(by.id("InsuranceNav")).tap();
		await expect(element(by.id("InsuranceScreen"))).toBeVisible();
	});
	//PERKS
	it("Render PerksScreen test", async () => {
		await element(by.id("PerksNav")).tap();
		await expect(element(by.id("PerksScreen"))).toBeVisible();
	});
	for (let i = 0; i < 5; i++) {
		it("PerksScreen Switch on sub menu " + i + " test", async () => {
			await element(by.id("togglePerks")).tap();
			await expect(element(by.id("perksOptions"))).toBeVisible();
			await element(by.id("perksOptions_" + i)).tap();
			await expect(element(by.id("perksOptions"))).not.toBeVisible();
			await expect(element(by.id("PerksScreen"))).toBeVisible();
		});
	}
	//CLAIMS
	it("Render ClaimsScreen test", async () => {
		await element(by.id("ClaimsNav")).tap();
		await expect(element(by.id("ClaimsScreen"))).toBeVisible();
	});
	//PRODUCTS
	it("Render ProductsScreen test", async () => {
		await element(by.id("ProductsNav")).tap();
		await expect(element(by.id("ProductsScreen"))).toBeVisible();
	});
	//ARTISANS
	it("Render ArtisansScreen test", async () => {
		await element(by.id("ArtisansNav")).tap();
		await expect(element(by.id("ArtisansScreen"))).toBeVisible();
	});
	//REALTIES
	it("Render RealtiesScreen test", async () => {
		await element(by.id("RealtiesNav")).tap();
		await expect(element(by.id("RealtiesScreen"))).toBeVisible();
	});
	it("RealtiesScreen case normal search test", async () => {
		await element(by.id("RealtiesNormalSearchBar")).typeText("house\n");
		await expect(element(by.id("RealtiesNormalSearchBar"))).toHaveText("house");
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case reset search test", async () => {
		await element(by.id("RealtiesResetNormalSearch")).tap();
		await expect(element(by.id("RealtiesNormalSearchBar"))).toHaveText("");
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case advance search open", async () => {
		await element(by.id("RealtiesOpenAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesNormalSearch")))
			.not.toBeVisible()
			.withTimeout(500);
		await waitFor(element(by.id("RealtiesAdvanceSearch")))
			.toBeVisible()
			.withTimeout(500);
	});
	it("RealtiesScreen case set search then filter test", async () => {
		await element(by.id("RealtiesAdvanceSearchBar")).typeText("apartment\n");
		await expect(element(by.id("RealtiesAdvanceSearchBar"))).toHaveText("apartment");
		await element(by.id("RealtiesFilterAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case set min price then filter test", async () => {
		await element(by.id("RealtiesAdvanceMinPrice")).typeText("1000000\n");
		await expect(element(by.id("RealtiesAdvanceMinPrice"))).toHaveText("1000000");
		await element(by.id("RealtiesFilterAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case set max price then filter test", async () => {
		await element(by.id("RealtiesAdvanceMaxPrice")).typeText("2000000\n");
		await expect(element(by.id("RealtiesAdvanceMaxPrice"))).toHaveText("2000000");
		await element(by.id("RealtiesFilterAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case set min beds then filter test", async () => {
		await element(by.id("RealtiesAdvanceMinBeds")).typeText("1\n");
		await expect(element(by.id("RealtiesAdvanceMinBeds"))).toHaveText("1");
		await element(by.id("RealtiesFilterAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case set max beds then filter test", async () => {
		await element(by.id("RealtiesAdvanceMaxBeds")).typeText("3\n");
		await expect(element(by.id("RealtiesAdvanceMaxBeds"))).toHaveText("3");
		await element(by.id("RealtiesFilterAdvanceSearch")).tap();
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case reset test", async () => {
		await element(by.id("RealtiesResetAdvanceSearch")).tap();
		await expect(element(by.id("RealtiesAdvanceSearchBar"))).toHaveText("");
		await expect(element(by.id("RealtiesAdvanceMinPrice"))).toHaveText("");
		await expect(element(by.id("RealtiesAdvanceMaxPrice"))).toHaveText("");
		await expect(element(by.id("RealtiesAdvanceMinBeds"))).toHaveText("");
		await expect(element(by.id("RealtiesAdvanceMaxBeds"))).toHaveText("");
		await waitFor(element(by.id("RealtiesList")))
			.toBeVisible()
			.withTimeout(5000);
	});
	it("RealtiesScreen case close test", async () => {
		await element(by.id("RealtiesCloseAdvanceSearch")).tap();
		await expect(element(by.id("RealtiesAdvanceSearch"))).not.toBeVisible();
		await expect(element(by.id("RealtiesNormalSearch"))).toBeVisible();
	});
	//MENU
	it("Render MenuScreen test", async () => {
		await element(by.id("MenuNav")).tap();
		await expect(element(by.id("MenuScreen"))).toBeVisible();
	});
	//TEST BACK TO HOME
	it("Render HomeScreen test", async () => {
		await element(by.id("HomeNav")).tap();
		await expect(element(by.id("HomeScreen"))).toBeVisible();
	});
};
describe("Luko App En", () => {
	it("Switch Languages to En test", async () => {
		await element(by.id("SwitchLang")).tap();
		await expect(element(by.id("LangOptionsView"))).toBeVisible();
		await element(by.id("LangOptions_en")).tap();
		await expect(element(by.id("HomeScreen"))).toBeVisible();
	});
	testFn();
});
describe("Luko App Fr", () => {
	it("Switch Languages to Fr test", async () => {
		await element(by.id("SwitchLang")).tap();
		await expect(element(by.id("LangOptionsView"))).toBeVisible();
		await element(by.id("LangOptions_sp")).tap();
		await expect(element(by.id("HomeScreen"))).toBeVisible();
	});
	testFn();
});
