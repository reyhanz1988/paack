module.exports = [
	/*0*/
	{
		en: "Language",
		es: "Idioma",
	},

	/*1*/
	{
		en: "Email",
		es: "Correo electrónico",
	},

	/*2*/
	{
		en: "Password",
		es: "Contraseña",
	},

	/*3*/
	{
		en: "Login",
		es: "Acceso",
	},

	/*4*/
	{
		en: "Save up to €15,000",
		es: "Économisez jusqu'à €15,000",
	},

	/*5*/
	{
		en: "Find the price of your mortgage insurance in 5 minutes",
		es: "Trouvez le prix de votre assurance de prêt immobilier en 5 minutes",
	},

	/*6*/
	{
		en: "Get your price",
		es: "Obtenez votre prix",
	},

	/*7*/
	{
		en: "Realty",
		es: "Immobilier",
	},

	/*8*/
	{
		en: "Find your future home match. Discover Immo, Luko`s home search feature - à la Tinder",
		es: "Trouvez votre futur match à domicile. Découvrez Immo, la fonction de recherche de logement de Luko - à la Tinder",
	},

	/*9*/
	{
		en: "Discover Realty",
		es: "Découvrez l'immobilier",
	},

	/*10*/
	{
		en: "Gas with Gazpar",
		es: "Gaz avec Gazpar",
	},

	/*11*/
	{
		en: "Power with Linky",
		es: "Puissance avec Linky",
	},

	/*12*/
	{
		en: "Luko Water",
		es: "Eau Luko",
	},
];
