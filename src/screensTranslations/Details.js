module.exports = [
	/*0*/
	{
		en: "Packet Details",
		es: "Detalles del paquete",
	},

	/*1*/
	{
		en: "Deliver",
		es: "Entregar",
	},

    /*2*/
	{
		en: "Due Date : ",
		es: "Fecha de vencimiento : ",
	},
];
