module.exports =  [
/*0*/
{
    en: "Menu",
    es: "Menu",

},

/*1*/
{
    en: "Profile",
    es: "Perfil",

},

/*2*/
{
    en: "FAQ",
    es: "Preguntas más frecuentes",

},

/*3*/
{
    en: "Privacy Policy",
    es: "Política de privacidad",

},

/*4*/
{
    en: "Logout",
    es: "Cerrar sesión",

},
];