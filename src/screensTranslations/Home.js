module.exports = [
	/*0*/
	{
		en: "Home",
		es: "Casa",
	},

	/*1*/
	{
		en: "Hello, below is the packets to deliver.",
		es: "Hola, a continuación se muestran los paquetes a entregar.",
	},

	/*2*/
	{
		en: "Details",
		es: "Detalles",
	},

	/*3*/
	{
		en: "Deliver",
		es: "Entregar",
	},
];
