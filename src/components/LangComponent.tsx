import React, { useContext, useState } from "react";
import { LangContext } from "../../App";
import { TouchableOpacity, View } from "react-native";
import CountryFlag from "react-native-country-flag";
import AsyncStorage from "@react-native-async-storage/async-storage";

const LangComponent: React.FC = () => {
	const { currentLang } = useContext(LangContext);
	const { setCurrentLang } = useContext(LangContext);
	const [toggleLang, setSwitchLang] = useState(false);
	const changeLang = (nextLang) => {
		AsyncStorage.setItem("LANG", nextLang).then(() => {
			setCurrentLang(nextLang);
			setSwitchLang(!toggleLang);
		});
	};
	const LangList = [];
	LangList["en"] = "gb";
	LangList["es"] = "es";
	const SelectedLang = (
		<View>
			<TouchableOpacity testID="SwitchLang" onPress={() => setSwitchLang(!toggleLang)}>
				<CountryFlag isoCode={LangList[currentLang]} size={24} />
			</TouchableOpacity>
		</View>
	);
	const LangOptions = [];
	if (toggleLang) {
		for (const i in LangList) {
			let flagStyle = 0;
			if (i == "en") {
				flagStyle = 10;
			}
			LangOptions.push(
				<TouchableOpacity
					testID={"LangOptions_" + i}
					key={"LangComponent_" + i}
					style={{ marginRight: flagStyle }}
					onPress={() => changeLang(i)}>
					<CountryFlag isoCode={LangList[i]} size={24} />
				</TouchableOpacity>,
			);
		}
	}
	//RENDER
	return (
		<View>
			{SelectedLang}
			<View
				testID="LangOptionsView" 
				style={{
					position: "absolute",
					zIndex: 10,
					flexDirection: "row",
					right: 0,
				}}>
				{LangOptions}
			</View>
		</View>
	);
};

export default LangComponent;
