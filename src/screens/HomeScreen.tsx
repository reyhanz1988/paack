import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import * as productActions from "../redux/actions/productActions";
import { StyleSheet, ScrollView, View } from "react-native";
import { ActivityIndicator, Appbar, Button, Card, List, Divider } from "react-native-paper";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Home";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import PropTypes from "prop-types";

const HomeScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [loading, setLoading] = useState(true);
	const [getDataError, setGetDataError] = useState([]);
	const [user, setUser] = useState([]);
	const [packets, setPackets] = useState([]);
	useEffect(() => {
		setLoading(true);
		props.getUser();
		props.getPackets();
	}, []);
	//PROPS UPDATE USER
	useEffect(() => {
		setUser(props.getUserRes);
		setLoading(false);
	}, [props.getUserRes]);
	//PROPS UPDATE PACKETS
	useEffect(() => {
		setPackets(props.getPacketsRes);
		setGetDataError("");
		setLoading(false);
	}, [props.getPacketsRes]);
	if (loading) {
		//LOADING
		return (
			<View style={styles.wrapper} testID="HomeLoading">
				<ActivityIndicator size="large" />
			</View>
		);
	} else if (getDataError) {
		//ERROR HANDLING
		return (
			<View style={styles.wrapper} testID="HomeError">
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		//RENDER
		let userData = [];
		if (user) {
			userData = (
				<Card style={styles.userCard}>
					<Card.Content>
						<List.Item
							titleStyle={{ color: SECOND_COLOR, fontWeight: "bold" }}
							title={user.name}
							descriptionStyle={{ color: SECOND_COLOR }}
							description={languages[1][currentLang]}
						/>
					</Card.Content>
				</Card>
			);
		}
		const packetsList = [];
		if (packets.length > 0) {
			for (const i in packets) {
				packetsList.push(
					<Card key={"packets_" + i} style={styles.packetsCard}>
						<Card.Title
							title={(parseInt(i) + 1).toString() + ". " + packets[i].receipt_number}
						/>
						<Divider style={styles.theDivider} />
						<Card.Content>
							<List.Item
								title={packets[i].name}
								description={packets[i].phone + " | " + packets[i].email}
								left={(props) => <List.Icon {...props} icon={"account-box"} />}
								style={{
									borderBottomWidth: 1,
									borderBottomColor: "#ddd",
								}}
							/>
							<List.Item
								title={packets[i].province}
								description={
									packets[i].address +
									" | " +
									packets[i].country +
									", " +
									packets[i].province +
									", " +
									packets[i].city +
									", " +
									packets[i].zipcode
								}
								left={(props) => <List.Icon {...props} icon={"map-marker"} />}
								style={{
									borderBottomWidth: 1,
									borderBottomColor: "#ddd",
								}}
							/>
						</Card.Content>
						<Card.Actions style={styles.packetActions}>
							<Button
								uppercase={false}
								style={styles.actionButtons}
								mode="contained"
								onPress={() => {
									props.navigation.navigate("Details", { packetId: i });
								}}>
								{languages[2][currentLang]}
							</Button>
							<Button
								uppercase={false}
								style={styles.actionButtons}
								mode="contained"
								onPress={() => {
									console.log("test");
								}}>
								{languages[3][currentLang]}
							</Button>
						</Card.Actions>
					</Card>,
				);
			}
		}
		return (
			<View style={styles.wrapper} testID="HomeScreen">
				<Appbar.Header>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				<View style={styles.userView}>{userData}</View>
				<ScrollView style={styles.scrollWrapper}>{packetsList}</ScrollView>
			</View>
		);
	}
};

HomeScreen.propTypes = {
	getUser: PropTypes.func,
	getUserRes: PropTypes.any,
	getPackets: PropTypes.func,
	getPacketsRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type HomeScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	userView: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
	},
	userCard: {
		backgroundColor: FIRST_COLOR,
	},
	scrollWrapper: {
		marginBottom: 20,
	},
	packetsCard: {
		margin: 20,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	packetActions: {
		justifyContent: "flex-end",
	},
	actionButtons: {
		flex: 1,
		margin: 5,
	},
});

function mapStateToProps(props) {
	return {
		getUserRes: props.accountReducer.getUserRes,
		getPacketsRes: props.productReducer.getPacketsRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
