import React, { useContext, useEffect } from "react";
import { LoginContext, LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView, StyleSheet, View } from "react-native";
import { Appbar, List } from "react-native-paper";
import languages from "../screensTranslations/Menu";
import LangComponent from "../components/LangComponent";
import PropTypes from "prop-types";

const MenuScreen = (props) => {
	const { token } = useContext(LoginContext);
	const { setToken } = useContext(LoginContext);
	const { currentLang } = useContext(LangContext);
	const logoutRedux = () => {
		const vars = {};
		vars.token = token;
		vars.currentLang = currentLang;
		props.logoutRedux(vars);
	};
	//PROPS UPDATE
	useEffect(() => {
		if (props.logoutReduxRes && props.logoutReduxRes.status == "success") {
			AsyncStorage.clear().then(() => {
				setToken(null);
			});
		}
	}, [props.logoutReduxRes]);
	return (
		<View style={styles.wrapper} testID="MenuScreen">
			<Appbar.Header>
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[0][currentLang]}
				/>
				<LangComponent />
			</Appbar.Header>
			<ScrollView style={{ marginLeft: 10, marginRight: 10 }}>
				<List.Item
					title={languages[1][currentLang]}
					left={(props) => <List.Icon {...props} icon={"account"} />}
					right={(props) => <List.Icon {...props} icon="chevron-right" />}
					onPress={() => console.log("profile pressed")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
				<List.Item
					title={languages[2][currentLang]}
					left={(props) => <List.Icon {...props} icon={"frequently-asked-questions"} />}
					right={(props) => <List.Icon {...props} icon="chevron-right" />}
					onPress={() => console.log("faq pressed")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
				<List.Item
					title={languages[3][currentLang]}
					left={(props) => <List.Icon {...props} icon={"gavel"} />}
					right={(props) => <List.Icon {...props} icon="chevron-right" />}
					onPress={() => console.log("faq pressed")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
				<List.Item
					title={languages[4][currentLang]}
					left={(props) => <List.Icon {...props} icon={"logout"} />}
					right={(props) => <List.Icon {...props} icon="chevron-right" />}
					onPress={() => {
						logoutRedux();
					}}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
			</ScrollView>
		</View>
	);
};

MenuScreen.propTypes = {
	logoutRedux: PropTypes.func,
	logoutReduxRes: PropTypes.any,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type MenuScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

function mapStateToProps(state) {
	return {
		logoutReduxRes: state.accountReducer.logoutReduxRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);
