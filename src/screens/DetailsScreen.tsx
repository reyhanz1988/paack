import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import { StyleSheet, ScrollView, View } from "react-native";
import { ActivityIndicator, Appbar, Button, Card, List, Divider } from "react-native-paper";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Details";
import PropTypes from "prop-types";

const DetailsScreen = (props) => {
	const { packetId } = props.route.params;
	const { currentLang } = useContext(LangContext);
	const [loading, setLoading] = useState(true);
	const [getDataError, setGetDataError] = useState([]);
	const [packets, setPackets] = useState([]);
	useEffect(() => {
		setLoading(true);
		props.getPackets();
	}, []);
	//PROPS UPDATE PACKETS
	useEffect(() => {
		setPackets(props.getPacketsRes);
		setGetDataError("");
		setLoading(false);
	}, [props.getPacketsRes]);
	if (loading) {
		//LOADING
		return (
			<View style={styles.wrapper} testID="DetailsLoading">
				<ActivityIndicator size="large" />
			</View>
		);
	} else if (getDataError) {
		//ERROR HANDLING
		return (
			<View style={styles.wrapper} testID="DetailsError">
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		//RENDER
		let packetsList = [];
		if (packets.length > 0) {
			packetsList = (
				<Card style={styles.packetsCard}>
					<Card.Title title={packets[packetId].receipt_number} />
					<Divider style={styles.theDivider} />
					<Card.Content>
						<List.Item
							title={packets[packetId].name}
							left={(props) => <List.Icon {...props} icon={"account-box"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={packets[packetId].email}
							left={(props) => <List.Icon {...props} icon={"email"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={packets[packetId].phone}
							left={(props) => <List.Icon {...props} icon={"phone"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={"PO : " + packets[packetId].po}
							left={(props) => <List.Icon {...props} icon={"calendar"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={languages[2][currentLang] + "" + packets[packetId].duedate}
							left={(props) => <List.Icon {...props} icon={"calendar"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={packets[packetId].gender}
							left={(props) => <List.Icon {...props} icon={"gender-male-female"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
						<List.Item
							title={packets[packetId].province}
							description={
								packets[packetId].address +
								" | " +
								packets[packetId].country +
								", " +
								packets[packetId].province +
								", " +
								packets[packetId].city +
								", " +
								packets[packetId].zipcode
							}
							left={(props) => <List.Icon {...props} icon={"map-marker"} />}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: "#ddd",
							}}
						/>
					</Card.Content>
					<Card.Actions style={styles.packetActions}>
						<Button
							uppercase={false}
							style={styles.actionButtons}
							mode="contained"
							onPress={() => {
								console.log("test");
							}}>
							{languages[1][currentLang]}
						</Button>
					</Card.Actions>
				</Card>
			);
		}
		return (
			<View style={styles.wrapper} testID="DetailsScreen">
				<Appbar.Header>
					<Appbar.Action icon="arrow-left" onPress={() => props.navigation.goBack()} />
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				<ScrollView style={styles.scrollWrapper}>{packetsList}</ScrollView>
			</View>
		);
	}
};

DetailsScreen.propTypes = {
	getPackets: PropTypes.func,
	getPacketsRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
		goBack: PropTypes.func,
	}),
	route: PropTypes.shape({
		params: PropTypes.string,
	}),
};

type DetailsScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	scrollWrapper: {
		marginBottom: 20,
	},
	packetsCard: {
		margin: 20,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	packetActions: {
		justifyContent: "flex-end",
	},
	actionButtons: {
		flex: 1,
		margin: 5,
	},
});

function mapStateToProps(props) {
	return {
		getPacketsRes: props.productReducer.getPacketsRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);
