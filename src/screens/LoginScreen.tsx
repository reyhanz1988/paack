import React, { useContext, useEffect, useState } from "react";
import { LoginContext, LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Text, TextInput } from "react-native-paper";
import CountryFlag from "react-native-country-flag";
import logo from "../assets/images/logo.png";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/Login";
import PropTypes from "prop-types";

const LoginScreen = (props) => {
	const { setToken } = useContext(LoginContext);
	const { currentLang } = useContext(LangContext);
	const { setCurrentLang } = useContext(LangContext);
	const [email, setEmail] = React.useState("");
	const [password, setPassword] = React.useState("");
	const [getDataError, setGetDataError] = useState(null);
	const changeLang = (nextLang) => {
		AsyncStorage.setItem("LANG", nextLang).then(() => {
			setCurrentLang(nextLang);
		});
	};
	const LangList = [];
	LangList["en"] = "gb";
	LangList["es"] = "es";
	const LangOptions = [];
	for (const i in LangList) {
		let flagMargin = 0;
		if (i == "en") {
			flagMargin = 10;
		}
		let flagStyle;
		if (i == currentLang) {
			flagStyle = FIRST_COLOR;
		} else {
			flagStyle = SECOND_COLOR;
		}
		LangOptions.push(
			<TouchableOpacity
				testID={"LangOptions_" + i}
				key={"LangComponent_" + i}
				style={{
					marginRight: flagMargin,
					backgroundColor: flagStyle,
					paddingTop: 20,
					paddingBottom: 20,
					paddingLeft: 10,
					paddingRight: 10,
					borderRadius: 10,
					justifyContent: "center",
				}}
				onPress={() => changeLang(i)}>
				<CountryFlag isoCode={LangList[i]} size={24} />
			</TouchableOpacity>,
		);
	}
	const checkLogin = () => {
		const vars = {};
		vars.currentLang = currentLang;
		vars.email = email;
		vars.password = password;
		props.checkLogin(vars);
	};
	//PROPS UPDATE
	useEffect(() => {
		if (props.checkLoginRes) {
			if (props.checkLoginRes.status == "error") {
				setGetDataError(props.checkLoginRes.msg);
			} else {
				AsyncStorage.setItem("TOKEN", props.checkLoginRes.token).then(() => {
					setGetDataError(null);
					setToken(props.checkLoginRes.token);
				});
			}
		}
	}, [props.checkLoginRes]);
	let errorView;
	if (getDataError) {
		errorView = <Text style={styles.error}>{getDataError}</Text>;
	}
	//RENDER
	return (
		<View style={styles.wrapper} testID="LoginScreen">
			<View style={styles.logoView}>
				<Image source={logo} style={styles.logo} />
			</View>
			<View style={styles.loginView}>
				<View style={styles.langView}>
					<Text style={styles.languageText}>{languages[0][currentLang]}</Text>
					{LangOptions}
				</View>
				<TextInput
					type="outlined"
					style={styles.textInput}
					autoCapitalize="none"
					keyboardType={"email-address"}
					label={languages[1][currentLang]}
					placeholder={languages[1][currentLang]}
					value={email}
					onChangeText={(text) => setEmail(text)}
				/>
				<TextInput
					type="outlined"
					style={styles.textInput}
					autoCapitalize="none"
					secureTextEntry={true}
					label={languages[2][currentLang]}
					placeholder={languages[2][currentLang]}
					value={password}
					onChangeText={(text) => setPassword(text)}
				/>
				<Button
					uppercase={false}
					style={styles.loginButton}
					mode="contained"
					onPress={() => {
						checkLogin();
					}}>
					{languages[3][currentLang]}
				</Button>
				{errorView}
			</View>
		</View>
	);
};

LoginScreen.propTypes = {
	checkLogin: PropTypes.func,
	checkLoginRes: PropTypes.any,
};
type LoginScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		backgroundColor: FIRST_COLOR,
	},
	logoView: {
		marginTop: 40,
		marginBottom: 20,
		alignItems: "center",
		justifyContent: "center",
	},
	langView: {
		flexDirection: "row",
		height: 30,
		marginBottom: 20,
		justifyContent: "center",
	},
	languageText: {
		marginLeft: 5,
		flex: 1,
		textAlign: "left",
	},
	logo: {
		resizeMode: "contain",
		height: 60,
	},
	loginView: {
		borderRadius: 10,
		padding: 20,
		margin: 20,
		backgroundColor: SECOND_COLOR,
	},
	textInput: {
		marginBottom: 20,
	},
	loginButton: {
		height: 60,
		justifyContent: "center",
	},
	error: {
		backgroundColor: "red",
		color: "#ffffff",
		padding: 10,
		borderRadius: 5,
		marginTop: 10,
	},
});

function mapStateToProps(state) {
	return {
		checkLoginRes: state.accountReducer.checkLoginRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
