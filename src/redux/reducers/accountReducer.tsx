import { CHECK_LOGIN, GET_USER, LOGOUT_REDUX } from "../actions/accountActions";

const initialState = {
	successMsg: "",
	errorMsg: "",
	checkLoginRes: [],
	getUserRes: [],
	logoutReduxRes: [],
};

const accountReducer = (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  CHECK_LOGIN                                                                                                                     		*/
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case CHECK_LOGIN.REQUESTED:
			return { ...state };
		case CHECK_LOGIN.SUCCESS:
			return {
				...state,
				checkLoginRes: payload.response,
				errorMsg: "",
			};
		case CHECK_LOGIN.ERROR:
			return { ...state, errorMsg: payload.error };
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_USER                                                                                                                            	*/
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_USER.REQUESTED:
			return { ...state };
		case GET_USER.SUCCESS:
			return {
				...state,
				getUserRes: payload.response,
				errorMsg: "",
			};
		case GET_USER.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  LOGOUT                                                                                                                                  */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case LOGOUT_REDUX.REQUESTED:
			return { ...state };
		case LOGOUT_REDUX.SUCCESS:
			return {
				...state,
				logoutReduxRes: payload.response,
				errorMsg: "",
			};
		case LOGOUT_REDUX.ERROR:
			return { ...state, errorMsg: payload.error };
		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		default:
			return state;
	}
};

export default accountReducer;
