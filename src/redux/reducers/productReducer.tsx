import { GET_ARTISANS, GET_CLAIMS, GET_PACKETS, GET_PERKS, GET_PRODUCTS, GET_REALTIES } from "../actions/productActions";

const initialState = {
	successMsg: "",
	errorMsg: "",
	getArtisansRes: [],
	getClaimsRes: [],
	getPacketsRes: [],
	getPerksRes: [],
	getProductsRes: [],
	getRealtiesRes: [],
};

const productReducer = (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_ARTISANS                                                                                                                            */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_ARTISANS.REQUESTED:
			return { ...state };
		case GET_ARTISANS.SUCCESS:
			return {
				...state,
				getArtisansRes: payload.response,
				errorMsg: "",
			};
		case GET_ARTISANS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_CLAIMS                                                                                                                              */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_CLAIMS.REQUESTED:
			return { ...state };
		case GET_CLAIMS.SUCCESS:
			return {
				...state,
				getClaimsRes: payload.response,
				errorMsg: "",
			};
		case GET_CLAIMS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_PACKETS                                                                                                                             */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_PACKETS.REQUESTED:
			return { ...state };
		case GET_PACKETS.SUCCESS:
			return {
				...state,
				getPacketsRes: payload.response,
				errorMsg: "",
			};
		case GET_PACKETS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_PERKS                                                                                                                               */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_PERKS.REQUESTED:
			return { ...state };
		case GET_PERKS.SUCCESS:
			return {
				...state,
				getPerksRes: payload.response,
				errorMsg: "",
			};
		case GET_PERKS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_PRODUCTS                                                                                                                            */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_PRODUCTS.REQUESTED:
			return { ...state };
		case GET_PRODUCTS.SUCCESS:
			return {
				...state,
				getProductsRes: payload.response,
				errorMsg: "",
			};
		case GET_PRODUCTS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_REALTIES                                                                                                                            */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_REALTIES.REQUESTED:
			return { ...state };
		case GET_REALTIES.SUCCESS:
			return {
				...state,
				getRealtiesRes: payload.response,
				errorMsg: "",
			};
		case GET_REALTIES.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		default:
			return state;
	}
};

export default productReducer;
