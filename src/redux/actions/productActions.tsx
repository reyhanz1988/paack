import ProductApi from "../api/ProductApi";
import {
	createAsyncActionType,
	//errorToastMessage,
} from "../../components/util";

// getArtisans
export const GET_ARTISANS = createAsyncActionType("GET_ARTISANS");
export const getArtisans = () => (dispatch) => {
	dispatch({
		type: GET_ARTISANS.REQUESTED,
	});
	const response = ProductApi.getArtisans();
	dispatch({
		type: GET_ARTISANS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getArtisans()
		.then((response) => {
			dispatch({
				type: GET_ARTISANS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_ARTISANS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getClaims
export const GET_CLAIMS = createAsyncActionType("GET_CLAIMS");
export const getClaims = () => (dispatch) => {
	dispatch({
		type: GET_CLAIMS.REQUESTED,
	});
	const response = ProductApi.getClaims();
	dispatch({
		type: GET_CLAIMS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getClaims()
		.then((response) => {
			dispatch({
				type: GET_CLAIMS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_CLAIMS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getPackets
export const GET_PACKETS = createAsyncActionType("GET_PACKETS");
export const getPackets = () => (dispatch) => {
	dispatch({
		type: GET_PACKETS.REQUESTED,
	});
	const response = ProductApi.getPackets();
	dispatch({
		type: GET_PACKETS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getPackets()
		.then((response) => {
			dispatch({
				type: GET_PACKETS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_PACKETS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getPerks
export const GET_PERKS = createAsyncActionType("GET_PERKS");
export const getPerks = () => (dispatch) => {
	dispatch({
		type: GET_PERKS.REQUESTED,
	});
	const response = ProductApi.getPerks();
	dispatch({
		type: GET_PERKS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getPerks()
		.then((response) => {
			dispatch({
				type: GET_PERKS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_PERKS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getProducts
export const GET_PRODUCTS = createAsyncActionType("GET_PRODUCTS");
export const getProducts = () => (dispatch) => {
	dispatch({
		type: GET_PRODUCTS.REQUESTED,
	});
	const response = ProductApi.getProducts();
	dispatch({
		type: GET_PRODUCTS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getProducts()
		.then((response) => {
            dispatch({
				type: GET_PRODUCTS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_PRODUCTS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getRealties
export const GET_REALTIES = createAsyncActionType("GET_REALTIES");
export const getRealties = () => (dispatch) => {
	dispatch({
		type: GET_REALTIES.REQUESTED,
	});
	const response = ProductApi.getRealties();
	dispatch({
		type: GET_REALTIES.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getRealties()
		.then((response) => {
            dispatch({
				type: GET_REALTIES.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_REALTIES.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};