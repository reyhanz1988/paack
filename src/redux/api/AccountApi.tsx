//import { BASE_URL } from "@env";
import { User } from "./FakeData";

const AccountApi = {
	checkLogin(vars) {
		/*const params = {
			method: "POST",
			headers: {
				"Accept-Language": vars.currentLang,
				Accept: "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: vars.email,
				password: vars.password,
			}),
		};*/
		let data;
		if (vars.email == "reyhanz1988@gmail.com" && vars.password == "admin") {
			data = {
				status: "success",
				token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2NDQyNjc5MzMsImV4cCI6MTY3NTgwMzkzMywiYXVkIjoicGFhY2suY28iLCJzdWIiOiJyZXloYW56MTk4OEBnbWFpbC5jb20iLCJHaXZlbk5hbWUiOiJSZXloYW4iLCJTdXJuYW1lIjoiQWZmYW5kaWUiLCJFbWFpbCI6InJleWhhbnoxOTg4QGdtYWlsLmNvbSIsIlJvbGUiOiJBZG1pbiJ9.raEY-MtSXvNgtDQSDUa1OdBctIWSpePANwMtmQYGxqg",
				msg: "",
			};
		} else {
			let msg;
			if (vars.currentLang == "en") {
				msg = "email or password are invalid.";
			} else {
				msg = "el correo electrónico o la contraseña no son válidos.";
			}
			data = {
				status: "error",
				token: "",
				msg: msg,
			};
		}
		return data;
	},
	getUser() {
		return User;
	},
	logoutRedux(/*vars*/) {
		/*const params = {
			method: "DELETE",
			headers: {
				"Accept-Language": vars.currentLang,
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + vars.token,
			},
		};*/
		const data = {
			status: "success",
			token: "",
			msg: "",
		};
		return data;
	},
};

module.exports = AccountApi;
