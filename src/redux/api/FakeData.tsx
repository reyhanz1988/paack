const Artisans = [
	{
		id: 1,
		icon: "volume-off",
		title: {
			en: "My floor is squeaking",
			fr: "Mon parquet grince",
		},
		description: {
			en: "A quick and simple solution",
			fr: "Une solution simple et rapide",
		},
	},
	{
		id: 2,
		icon: "format-paint",
		title: {
			en: "Repaint a wall",
			fr: "Repeindre un mur",
		},
		description: {
			en: "Time for a new look",
			fr: "C'est l'heure d'un nouveau look",
		},
	},
	{
		id: 3,
		icon: "water-off",
		title: {
			en: "Cleanse your radiators and your hot water tank",
			fr: "Assainissez vos radiateurs et votre ballon d'eau chaude",
		},
		description: {
			en: "Once a year to avoid malfunction",
			fr: "Une fois par an pour éviter les dysfonctionnements",
		},
	},
	{
		id: 4,
		icon: "swap-horizontal",
		title: {
			en: "Change my tile seals",
			fr: "Changer mes joints de carrelage",
		},
		description: {
			en: "If they aren't waterproof anymore",
			fr: "S'ils ne sont plus étanches",
		},
	},
	{
		id: 5,
		icon: "door-open",
		title: {
			en: "My front door is squeaking",
			fr: "Ma porte d'entrée grince",
		},
		description: {
			en: "Among other solutions, olive oil !",
			fr: "Entre autres solutions, l'huile d'olive !",
		},
	},
	{
		id: 6,
		icon: "water",
		title: {
			en: "Install / remove a siphon",
			fr: "Installer / retirer un siphon",
		},
		description: {
			en: "First thing in case of clogged sink",
			fr: "Première chose en cas d'évier bouché",
		},
	},
	{
		id: 7,
		icon: "door-open",
		title: {
			en: "What should I do if my door scrapes on the floor?",
			fr: "Que dois-je faire si ma porte racle le sol ?",
		},
		description: {
			en: "Easier than you think",
			fr: "Plus facile que vous ne le pensez",
		},
	},
	{
		id: 8,
		icon: "ceiling-light",
		title: {
			en: "Change for eco-friendly light bulbs",
			fr: "Changer pour des ampoules écologiques",
		},
		description: {
			en: "A practical guide for choosing and installing them",
			fr: "Un guide pratique pour les choisir et les installer",
		},
	},
	{
		id: 9,
		icon: "window-closed-variant",
		title: {
			en: "Temporarily repairing my broken window",
			fr: "Réparer temporairement ma fenêtre cassée",
		},
		description: {
			en: "First aid in case of glass breakage",
			fr: "Premiers secours en cas de bris de glace",
		},
	},
	{
		id: 10,
		icon: "crosshairs-gps",
		title: {
			en: "Fix a small hole in a wall",
			fr: "Réparer un petit trou dans un mur",
		},
		description: {
			en: "Guarantee to get your deposit back",
			fr: "Garantie de récupérer votre acompte",
		},
	},
];

const Claims = [
	{
		id: 1,
		title: {
			en: "Claim report",
			fr: "Rapport de réclamation",
		},
		description: {
			en: "Describe what happened via the application: this is the way of reporting a claim.",
			fr: "Décrivez ce qui s'est passé via l'application : c'est la manière de déclarer un sinistre.",
		},
	},
	{
		id: 2,
		title: {
			en: "Damage report",
			fr: "Rapport de dommages",
		},
		description: {
			en: "Film the damage to help our experts assess the amount of damage.",
			fr: "Filmez les dégâts pour aider nos experts à évaluer le montant des dégâts.",
		},
	},
	{
		id: 3,
		title: {
			en: "Adding invoices",
			fr: "Ajouter des factures",
		},
		description: {
			en: "If necessary, include photos and invoices of the damaged goods with your report.",
			fr: "Si nécessaire, joignez des photos et des factures des marchandises endommagées à votre rapport.",
		},
	},
	{
		id: 4,
		title: {
			en: "Refund",
			fr: "Rembourser",
		},
		description: {
			en: "You will receive your compensation on average twice as fast as with a traditional insurer, by bank transfer or instant Lydia transfer.",
			fr: "Vous recevrez votre indemnisation en moyenne deux fois plus vite qu'avec un assureur traditionnel, par virement bancaire ou virement instantané Lydia.",
		},
	},
];

const Perks = [
	{
		id: 1,
		title: {
			en: "Services",
			fr: "Prestations de service",
		},
		link: "Services",
		perks: [
			{
				id: 1,
				title: "Papernest",
				description: {
					en: "papernest supports you for free in dealing with your contracts for your new home energy, internet, mobile, moving, mail",
					fr: "papernest vous accompagne gratuitement dans la gestion de vos contrats pour votre maison neuve énergie, internet, mobile, déménagement, courrier",
				},
				perks: {
					en: "€30 cashback on your Luko contract",
					fr: "€30 de cashback sur votre contrat Luko",
				},
			},
			{
				id: 2,
				title: "Garantme",
				description: {
					en: "For tenants with no one to stand as guarantor",
					fr: "Pour les locataires n'ayant personne pour se porter garant",
				},
				perks: {
					en: "€35 off",
					fr: "€35 de réduction",
				},
			},
			{
				id: 3,
				title: "Des Bras En Plus",
				description: {
					en: "French leader in online professional movers booking service. Get a quote in 5 minutes!",
					fr: "Leader français de la réservation en ligne de déménageurs professionnels. Obtenez un devis en 5 minutes !",
				},
				perks: {
					en: "5% off",
					fr: "5% de réduction",
				},
			},
			{
				id: 4,
				title: "Nextories",
				description: {
					en: "Nextories supports you in the organisation of your relocation with a free, personalised, transparent and quality service.",
					fr: "Nextories vous accompagne dans l'organisation de votre déménagement avec un service gratuit, personnalisé, transparent et de qualité.",
				},
				perks: {
					en: "2h of free cleaning",
					fr: "2h de ménage gratuit",
				},
			},
			{
				id: 5,
				title: "Avostart",
				description: {
					en: "Avostart is a service that puts you in touch with lawyers who will answer all your legal questions.",
					fr: "Avostart est un service qui vous met en relation avec des avocats qui répondront à toutes vos questions juridiques.",
				},
				perks: {
					en: "10% off",
					fr: "10% de réduction",
				},
			},
			{
				id: 6,
				title: "Bâti-Assure",
				description: {
					en: "Simplifies for your the procedure to get a structural damage insurance",
					fr: "Simplifie pour vous la procédure d'obtention d'une assurance dommage structure",
				},
				perks: {
					en: "1 instant quote",
					fr: "1 devis immédiat",
				},
			},
			{
				id: 7,
				title: "Algar",
				description: {
					en: "Deal with your processes of construction permit and statement of work for you",
					fr: "Traiter pour vous vos démarches de permis de construire et de cahier des charges",
				},
				perks: {
					en: "15% off on urban planning procedure management",
					fr: "15% de réduction sur la gestion des procédures d'urbanisme",
				},
			},
		],
	},
	{
		id: 2,
		title: {
			en: "Real Estate",
			fr: "Immobilier",
		},
		link: "RealEstate",
		perks: [
			{
				id: 8,
				title: "Hosman",
				description: {
					en: "Hosman is the neo-agency that is revolutionising real estate transactions.",
					fr: "Hosman est la néo-agence qui révolutionne les transactions immobilières.",
				},
				perks: {
					en: "€100 off",
					fr: "€100 de réduction",
				},
			},
			{
				id: 9,
				title: "Matera",
				description: {
					en: "With Matera, take back your condominium control and save 30% on your charges",
					fr: "Avec Matera, reprenez le contrôle de votre copropriété et économisez 30% sur vos charges",
				},
				perks: {
					en: "1 month of free charges for the condomnium + 1 trimester of free charges for the first insured person of the condominium",
					fr: "1 mois de charges gratuites pour la copropriété + 1 trimestre de charges gratuites pour le premier assuré de la copropriété",
				},
			},
			{
				id: 10,
				title: "Flatlooker",
				description: {
					en: "Flatlooker is the digital real estate agency specialized in rental and management.",
					fr: "Flatlooker est l'agence immobilière digitale spécialisée dans la location et la gestion.",
				},
				perks: {
					en: "2 months free on the rental management of your flat",
					fr: "2 mois offerts sur la gestion locative de votre appartement",
				},
			},
			{
				id: 11,
				title: "Settlesweet",
				description: {
					en: "Settlesweet is the professional hunter to find you the flat of your dreams.",
					fr: "Settlesweet est le chasseur professionnel pour vous trouver l'appartement de vos rêves.",
				},
				perks: {
					en: "€50 off",
					fr: "€50 de réduction",
				},
			},
		],
	},
	{
		id: 3,
		title: {
			en: "Energy",
			fr: "Énergie",
		},
		link: "Energy",
		perks: [
			{
				id: 12,
				title: "Hello Watt",
				description: {
					en: "Find a cheaper and renewable electricity or gas supplier for your accommodation.",
					fr: "Trouvez un fournisseur d'électricité ou de gaz moins cher et renouvelable pour votre logement.",
				},
				perks: {
					en: "1 month off on Luko contract",
					fr: "1 mois de remise sur le contrat Luko",
				},
			},
			{
				id: 13,
				title: "Oopla",
				description: {
					en: "Social and Solidarity Economy company offering simple and economical solutions to reduce our footprint on the planet.",
					fr: "Entreprise d'Economie Sociale et Solidaire proposant des solutions simples et économiques pour réduire notre empreinte sur la planète.",
				},
				perks: {
					en: "15% off",
					fr: "15% de réduction",
				},
			},
		],
	},
	{
		id: 4,
		title: {
			en: "Handiwork",
			fr: "Ouvrage",
		},
		link: "Handiwork",
		perks: [
			{
				id: 14,
				title: "Castorama",
				description: {
					en: "Major player in home improvement in France.",
					fr: "Acteur majeur de l'amélioration de l'habitat en France.",
				},
				perks: {
					en: "10% off & many more free services",
					fr: "10% de réduction et de nombreux autres services gratuits",
				},
			},
			{
				id: 15,
				title: "Izidore",
				description: {
					en: "Create your own flat-emptying in just a few clicks.",
					fr: "Créez votre propre vidage d'appartement en quelques clics.",
				},
				perks: {
					en: "Free estimate of the resale price of your furniture",
					fr: "Estimation gratuite du prix de revente de vos meubles",
				},
			},
			{
				id: 16,
				title: "Allovoisins",
				description: {
					en: "Join the 4 million individuals and professionals of AlloVoisins for the realization of all your projects!",
					fr: "Rejoignez les 4 millions de particuliers et professionnels d'AlloVoisins pour la réalisation de tous vos projets !",
				},
				perks: {
					en: "10% off",
					fr: "10% de réduction",
				},
			},
		],
	},
	{
		id: 5,
		title: {
			en: "Finance and Insurance",
			fr: "Finance et Assurance",
		},
		link: "FinanceAndInsurance",
		perks: [
			{
				id: 17,
				title: "Finfrog",
				description: {
					en: "Finfrog is a platform for loans to individuals.",
					fr: "Finfrog est une plateforme de prêts aux particuliers.",
				},
				perks: {
					en: "Microcredit at €1/month",
					fr: "Microcrédit à 1€/mois",
				},
			},
			{
				id: 18,
				title: "Coverd",
				description: {
					en: "100% online insurance policy for your smartphone.",
					fr: "Une assurance 100% en ligne pour votre smartphone.",
				},
				perks: {
					en: "1 month off",
					fr: "1 mois de repos",
				},
			},
			{
				id: 19,
				title: "Monese",
				description: {
					en: "Monese is a neobank with no limits or fees for all transfers, recharges and withdrawals.",
					fr: "Monese est une néobanque sans limites ni frais pour tous les transferts, recharges et retraits.",
				},
				perks: {
					en: "2 months off on 'Premium' pack",
					fr: "2 mois de réduction sur le pack 'Premium'",
				},
			},
			{
				id: 20,
				title: "Luko",
				description: {
					en: "Refer your friends or subscribe another contract!",
					fr: "Parrainez vos amis ou souscrivez un autre contrat !",
				},
				perks: {
					en: "1 month off",
					fr: "1 mois de repos",
				},
			},
		],
	},
];

const Products = [
	{
		id: 1,
		icon: "office-building",
		image: require("../../assets/images/products/flat.jpg"),
		title: {
			en: "Flat Insurance",
			fr: "Assurance Forfaitaire",
		},
		description: {
			en: "You live in a flat and you are wondering: what is the purpose of home insurance? Is it compulsory? Find all the answers and the guarantees adapted to your home.",
			fr: "Vous habitez en appartement et vous vous demandez : à quoi sert l'assurance habitation ? Est-ce obligatoire ? Retrouvez toutes les réponses et les garanties adaptées à votre habitation.",
		},
	},
	{
		id: 2,
		icon: "home",
		image: require("../../assets/images/products/home.jpg"),
		title: {
			en: "Home Insurance",
			fr: "Assurance Habitation",
		},
		description: {
			en: "Whether you live in a detached house, a condominium, or a chalet in the mountains, insuring your home allows you to protect your real estate capital as well as the members of your household against the hazards of life.",
			fr: "Que vous habitiez une maison individuelle, une copropriété ou un chalet à la montagne, assurer votre habitation vous permet de protéger votre capital immobilier ainsi que les membres de votre foyer contre les aléas de la vie.",
		},
	},
	{
		id: 3,
		icon: "umbrella",
		image: require("../../assets/images/products/secondaryhome.jpg"),
		title: {
			en: "Secondary Home Insurance",
			fr: "Assurance Habitation Secondaire",
		},
		description: {
			en: "You can leave your secondary home peacefully: Luko gives you all the details on the guarantees you need to know to protect your haven of peace.",
			fr: "Vous pouvez quitter votre résidence secondaire sereinement : Luko vous donne tous les détails sur les garanties à connaître pour protéger votre havre de paix.",
		},
	},
	{
		id: 4,
		icon: "home-city",
		image: require("../../assets/images/products/landlord.jpg"),
		title: {
			en: "Landlord Insurance",
			fr: "Assurance Propriétaire",
		},
		description: {
			en: "The landlord's insurance covers you when you own a rental investment. It is the insurance suited for you if you are a landlord or if your property is unoccupied.",
			fr: "L'assurance bailleur vous couvre lorsque vous êtes propriétaire d'un investissement locatif. C'est l'assurance qui vous convient si vous êtes propriétaire ou si votre bien est inoccupé.",
		},
	},
	{
		id: 5,
		icon: "office-building",
		image: require("../../assets/images/products/building.jpg"),
		title: {
			en: "Building Insurance",
			fr: "Assurance Bâtiment",
		},
		description: {
			en: "An insurance dedicated to condominiums and rental buildings owners to cover the building the owners (including the landlord insurance) and even the civil liability of benevolent property managers.",
			fr: "Une assurance dédiée aux propriétaires de copropriétés et d'immeubles locatifs pour couvrir l'immeuble des propriétaires (y compris l'assurance bailleur) et même la responsabilité civile des syndics bienveillants.",
		},
	},
];

const Realties = [
	{
		id: 1,
		image: require("../../assets/images/realty/1.jpg"),
		title: "4 Room(s) Apartment for Sale in Paris 12E Arrondissement",
		region: "Paris Île-de-France",
		department: "Paris (75)",
		location: "Paris, 75012",
		bedrooms: 3,
		bathrooms: 2,
		property_size: 94,
		price: 1215000,
	},
	{
		id: 2,
		image: require("../../assets/images/realty/2.jpg"),
		title: "4 Room(s) Apartment for Sale in Paris 11E Arrondissement",
		region: "Paris Île-de-France",
		department: "Paris (75)",
		location: "Paris, 75011",
		bedrooms: 3,
		bathrooms: 2,
		property_size: 116,
		price: 1295000,
	},
	{
		id: 3,
		image: require("../../assets/images/realty/3.jpg"),
		title: "4 Room(s) House for Sale in Paris 19E Arrondissement",
		region: "Paris Île-de-France",
		department: "Paris (75)",
		location: "Paris, 75019",
		bedrooms: 3,
		bathrooms: 2,
		property_size: 117,
		price: 1950000,
	},
	{
		id: 4,
		image: require("../../assets/images/realty/4.jpg"),
		title: "2 Room(s) Apartment for Sale in Paris 8E Arrondissement",
		region: "Paris Île-de-France",
		department: "Paris (75)",
		location: "Paris, 75008",
		bedrooms: 1,
		bathrooms: 1,
		property_size: 46,
		price: 749000,
	},
	{
		id: 5,
		image: require("../../assets/images/realty/5.jpg"),
		title: "2 Room(s) Apartment for Sale in Paris 08",
		region: "Paris Île-de-France",
		department: "Paris (75)",
		location: "Paris, 75008",
		bedrooms: 1,
		bathrooms: 1,
		property_size: 33,
		price: 509500,
	},
];

//https://www.fakexy.com/fake-address-generator-es
const Packets = [
	{
		id: 1,
		name: "Alexandra Font Segundo",
		email: "alexandra@gmail.com",
		phone: "912 03 05 00",
		gender: "Female",
		receipt_number: "paack-M-2022-00001",
		po: "2022-01-24",
		duedate: "2022-02-08",
		address: "Autovia A-1, Salida 19",
		city: "San Sebastian de los Reyes",
		province: "Madrid",
		country: "Spain",
		zipcode: "28700",
		lat: "40.545431",
		lon: "-3.613686",
	},
	{
		id: 2,
		name: "Luna Bustamante Tercero",
		email: "luna@gmail.com",
		phone: "916 19 27 66",
		gender: "Female",
		receipt_number: "paack-M-2022-00002",
		po: "2022-01-24",
		duedate: "2022-02-08",
		address: "Calle Porto Colon, 2 posterior",
		city: "Alcorcon",
		province: "Madrid",
		country: "Spain",
		zipcode: "28924",
		lat: "40.351813",
		lon: "-3.821531",
	},
	{
		id: 3,
		name: "Bruno Hernádez",
		email: "bruno@gmail.com",
		phone: "916 79 29 56",
		gender: "Male",
		receipt_number: "paack-M-2022-00003",
		po: "2022-01-24",
		duedate: "2022-02-08",
		address: "Calle del Marques de Hinojares, 25",
		city: "Mejorada del Campo",
		province: "Madrid",
		country: "Spain",
		zipcode: "28840",
		lat: "40.398076",
		lon: "-3488765.0",
	},
	{
		id: 4,
		name: "Diana Tafoya Tercero",
		email: "diana@gmail.com",
		phone: "912 48 38 02",
		gender: "Female",
		receipt_number: "paack-M-2022-00004",
		po: "2022-01-24",
		duedate: "2022-02-08",
		address: "Calle Asturias, s/n",
		city: "Pinto",
		province: "Madrid",
		country: "Spain",
		zipcode: "28320",
		lat: "40.249574",
		lon: "-3.693546",
	},
	{
		id: 5,
		name: "José Manuel Sedillo",
		email: "jose@gmail.com",
		phone: "902 23 23 50",
		gender: "Male",
		receipt_number: "paack-M-2022-00005",
		po: "2022-01-24",
		duedate: "2022-02-08",
		address: "Calle Tajo, s/n",
		city: "Villaviciosa de Odon",
		province: "Madrid",
		country: "Spain",
		zipcode: "28670",
		lat: "40.373927",
		lon: "-3.92074",
	},
];

const User = {
	id: 1,
	photo: require("../../assets/images/rey.png"),
	name: "Reyhan Emir Affandie",
	nick: "Reyhan",
	email: "reyhanz1988@gmail.com",
	phone: "938 20 02 51",
	gender: "Male",
	dob: "1988-06-17",
	ssn: "847-10-9194",
	address: "Carrer de la Creu, 16",
	city: "Balsareny",
	province: "Barcelona",
	country: "Spain",
	zipcode: "08660",
	vtype: "vans",
	vplate: "BG70AYA",
};

export { Artisans, Claims, Perks, Products, Realties, Packets, User };
