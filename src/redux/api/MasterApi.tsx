import { BASE_URL } from "@env";

const MasterApi = {
	getLang() {
		const params = {
			method: "GET",
			headers: {
				"Accept-Language": "en-US",
				Accept: "application/json",
				"Content-Type": "application/json",
			},
		};
		return fetch(BASE_URL + "/api/languages", params)
			.then((res) => res.json())
			.catch((error) => {
				console.warn("getLang => " + error);
				return "error";
			});
	},
};

module.exports = MasterApi;
