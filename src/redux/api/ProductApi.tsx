//import { API_URL } from "@env";
import { Artisans, Claims, Packets, Perks, Products, Realties } from "./FakeData";
const ProductApi = {
	getArtisans() {
		return Artisans;
	},
	getClaims() {
		return Claims;
	},
	getPackets() {
		return Packets;
	},
	getPerks() {
		return Perks;
	},
	getProducts() {
		return Products;
	},
	getRealties() {
		return Realties;
	},
};

module.exports = ProductApi;
