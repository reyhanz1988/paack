import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./src/redux/reducers/rootReducer";

export default createStore(rootReducer, applyMiddleware(thunk));
